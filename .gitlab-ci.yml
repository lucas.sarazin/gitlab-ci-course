stages:
    - build
    - test
    - quality
    - sonarqube-check
    - sonarqube-vulnerability-report
    - package

cache:
    paths:
        - .m2/repository
    key: "$CI_JOB_NAME"

build-job:
  stage: build
  script:
        - echo "Compiling the code..."
        - ./mvnw compile
          -Dhttps-protocols=TLSv1.2
          -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
          -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
          -Dorg.slf4j.simpleLogger.showDateTime=true
          -Djava.awt.headless=true--batch-mode --errors --fail-at-end --show-version 
        - echo "Compile complete."

test-job:
  stage: test
  script:
      - echo "Testing the code..."
      - ./mvnw test
        -Dhttps-protocols=TLSv1.2
        -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
        -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
        -Dorg.slf4j.simpleLogger.showDateTime=true
        -Djava.awt.headless=true--batch-mode --errors --fail-at-end --show-version 
      - echo "Test complete."
image: openjdk:8-alpine

sonarqube-check:
  stage: sonarqube-check
  image: maven:3-eclipse-temurin-17
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - mvn verify sonar:sonar
  allow_failure: true
  only:
    - merge_requests
    - master
    - main
    - develop

sonarqube-vulnerability-report:
  stage: sonarqube-vulnerability-report
  script:
    - 'curl -u "${SONAR_TOKEN}:" "${SONAR_HOST_URL}/api/issues/gitlab_sast_export?projectKey=lucas.sarazin_gitlab-ci-course_AYtwKzpo-5bISsW9YpWO&branch=${CI_COMMIT_BRANCH}&pullRequest=${CI_MERGE_REQUEST_IID}" -o gl-sast-sonar-report.json'
  allow_failure: true
  only:
    - merge_requests
    - master
    - main
    - develop
  artifacts:
    expire_in: 1 day
    reports:
      sast: gl-sast-sonar-report.json
  dependencies:
    - sonarqube-check

code-quality-job:
  stage: quality
  image: docker:stable
  variables:
    DOCKER_HOST: tcp://docker:2375/
    # Use the overlayfs driver for improved performance:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  allow_failure: true
  services:
    - name: docker:dind
      alias: docker
      entrypoint: ["dockerd-entrypoint.sh", "--tls=false"]
  script:
    - echo "Qualify the code..."
    - mkdir codequality-results
    - docker run
      --env CODECLIMATE_CODE="$PWD"
      --volume "$PWD":/code
      --volume /var/run/docker.sock:/var/run/docker.sock
      --volume /tmp/cc:/tmp/cc
      codeclimate/codeclimate analyze -f html > ./codequality-results/index.html
    - echo "Qualifying complete."
  artifacts:
    paths:
      - codequality-results/

package-job:
  stage: package
  variables:
    DOCKER_HOST: tcp://docker:2375/
    # Use the overlayfs driver for improved performance:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
  services:
    - name: docker:dind
      alias: docker
      entrypoint: ["dockerd-entrypoint.sh", "--tls=false"]
  script:
    - echo "Packaging the code..."
    - apk add --no-cache docker
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - ./mvnw install -PbuildDocker -DskipTests=true -DpushImage
      -Dhttps-protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true--batch-mode --errors --fail-at-end --show-version 
    - echo "Package complete."
  image: openjdk:8-alpine